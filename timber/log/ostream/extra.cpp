#include <timber/log/ostream/extra.hpp>

namespace timber::log {

void PrintExtra(std::ostream& out, const ExtraFields extra) {
  if (extra.empty()) {
    return;
  }

  out << "[";
  for (size_t i = 0; i < extra.size(); ++i) {
    out << extra[i].key << '=' << extra[i].value;
    if (i + 1 < extra.size()) {
      out << ", ";
    }
  }
  out << "]";
}

std::ostream& operator<<(std::ostream& out, WithExtra with_extra) {
  if (!with_extra.headers.empty()) {
    PrintExtra(out, with_extra.headers);
    out << ' ';
  }

  out << with_extra.message;

  if (!with_extra.trailers.empty()) {
    out << ' ';
    PrintExtra(out, with_extra.trailers);
  }

  return out;
}

}  // namespace timber::log
