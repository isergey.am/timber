#pragma once

#include <timber/log/level.hpp>
#include <timber/log/level/string.hpp>

#include <ostream>

namespace timber::log {

inline std::ostream& operator<<(std::ostream& out, Level level) {
  out << LevelToString(level);
  return out;
}

}  // namespace timber::log
