#pragma once

#include <timber/log/backend.hpp>

#include <timber/log/ostream/level.hpp>
#include <timber/log/ostream/extra.hpp>

#include <fmt/core.h>

namespace timber::log::backends {

// For examples
// Not thread-safe

class StdoutBackend : public IBackend {
 public:
  Level GetMinLevelFor(const std::string& /*component*/) const override {
    return Level::All;
  }

  void Log(Record record) override {
    fmt::print("{:<10} -- {:7} -- {}\n",
               record.component,
               record.level,
               timber::log::WithExtra{record.extra.logger, record.message, record.extra.site});
  }
};

}  // namespace timber::log::backends
