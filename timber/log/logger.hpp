#pragma once

#include <timber/log/backend.hpp>

#include <string>

namespace timber::log {

class Logger {
 public:
  Logger(std::string_view component, IBackend& backend)
      : backend_(backend), component_(component) {
  }

  Logger(std::string_view component);

  Logger& AddExtra(ExtraField field) {
    extra_.push_back(field);
    return *this;
  }

  bool IsLevelEnabled(Level level) const {
    return level >= backend_.GetMinLevelFor(component_);
  }

  void Log(Level level, std::string message) {
    backend_.Log({level, component_, std::move(message), {LoggerExtra(), SiteExtra()}});
  }

  IBackend& Backend() {
    return backend_;
  }

 private:
  ExtraFields LoggerExtra() {
    return extra_;
  }

  ExtraFields SiteExtra() {
    return backend_.CurrentSiteContext();
  }

 private:
  IBackend& backend_;
  std::string component_;
  ExtraFields extra_;
};

}  // namespace timber::log
