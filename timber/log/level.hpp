#pragma once

namespace timber::log {

enum class Level {
  All = 0,  // Do not format
  Trace = 1,
  Debug = 2,
  Info = 3,
  Warning = 4,
  Error = 5,
  Critical = 6,
  Off = 100500
};

}  // namespace timber::log
